# Encoding: utf-8

require 'csv'
require 'colorized_string'
require 'colorize'
require 'console_view_helper'

class Game

  #atrributes and accessors methods
  attr_accessor :total_shots, :failed_shots, :username, :board, :location_ships_destroyed, :board_result
  
  # builder
  def initialize(username)
    @total_shots = 0
    @failed_shots = 0
    @username = username
    @board = nil
    @board_result = nil
    @locations_ships_destroyed = []
  end

  # {config: :file, filename: filename} with_file
  # {config: :random, row=6, col=10, ships=3} with_random
  def load_board(setup)
    @board = Board.new
    @board.load({mode: setup[:config], filename: setup[:filename]})
    @board_result = Board.new
    @board_result.load({mode: setup[:config], filename: setup[:filename]})
  end

  def gun(mapa, pos)
    location_ship = []
    if mapa[pos[0]][pos[1]] == 'x'
      info_init_y = searchx(mapa, pos[0], pos[1], :left)
      info_end_y = searchx(mapa, pos[0], pos[1], :right)
      if info_init_y[:found] or info_end_y[:found] 
        (info_init_y[:target]..info_end_y[:target]).each {|j| location_ship << "#{pos[0]},#{j}"}
      else
        info_init_x = searchy(mapa, pos[0], pos[1], :up)
        info_end_x = searchy(mapa, pos[0], pos[1], :down)
        if info_init_x[:found] or info_end_x[:found]
          (info_init_x[:target]..info_end_x[:target]).each {|j| location_ship << "#{j},#{pos[1]}"}
        else
          location_ship << "#{pos[0]}, #{pos[1]}"
        end
      end
    end
    location_ship
  end
  
  def searchx(board, posx, posy, orie)
    config = {right: 1, left: -1}
    target = posy
    found = false
    tam_col = board[0].size
    if posy -1 >= -1 and posy + 1 <= tam_col
      i = posy + 1 * config[orie]
      sw = true
      while( i>= 0 and i<= tam_col-1 and sw)
        if board[posx][i] == 'x'
          found = true
          target = i
          i = i + 1 * config[orie]
        else
          sw = false
        end
      end
    end
    {target: target, found: found}
  end

  def searchy(board, posx, posy, orie)
    config = {down: 1, up: -1}
    target = posx
    found = false
    tam_row = board.size
    if posx -1 >= -1 and posx + 1 <= tam_row
      i = posx + 1 * config[orie]
      sw = true
      while( i>= 0 and i<= tam_row-1 and sw)
        if board[i][posy] == 'x'
          found = true
          target = i
          i = i + 1 * config[orie]
        else
          sw = false
        end
      end
    end
    {target: target, found: found}
  end

  def shoot(pos)
    found_ship = gun(@board.blocks, pos)
    @total_shots += 1
    unless found_ship.empty?  
      @locations_ships_destroyed << found_ship
      found_ship.each do |location|
        location = location.split(",").map(&:to_i)
        @board_result.blocks[location[0]][location[1]] = "1"
      end
    else
      @failed_shots += 1
    end
    found_ship
  end
end

class Board
  
  attr_reader :blocks, :row, :col, :ships
  
  def initialize(row=nil, col=nil, ships=nil)
    @blocks = []
    @row = row
    @col = col
    @ships = ships
  end

  def fill(base=0)
    matrix = []
    (0...@row).each do |i|
      line = []
      (0...@col).each do |j|
        line << base
      end
      matrix << line
    end
    matrix    
  end

  def load(config)
    case config[:mode]
    when :file
      load_csv(config[:filename])
    when :random
        #load_random
    end
  end
  
  def load_random
    @blocks = fill()
  end

  def load_csv(filename)
    @blocks = CSV.read(filename)
    @row = @blocks.size 
    @col = @blocks[0].size
  end
end

#------------------------------gui-------------------------------

class Gui
  
  attr_reader :game
  
  def initialize
    @game = nil
  end
  
  def layout
    system "clear"
    puts ConsoleViewHelper.banner('Battlefield La Haus', subtitle: 'By Roberto Daza :)' , symbol: '*', width: 50)
  end

  def option_layout
    print "#{@game.username}, digita una opcion"
    option = validate_precense_digit(ConsoleViewHelper.input(label = '>>', indent = 0), "opcion")
  end
  
  def initial_presentation
    layout()
    puts "Hola!!! Jugador, para empezar, porfavor digita tu nombre"
    username = validate_precense(ConsoleViewHelper.input(label = '>>', indent = 0), "nombre")
    @game = Game.new(username)
  end
  
  def menu
    layout()
    puts "Menu"
    puts "1. Cargar tablero"
    puts "2. Jugar"
    puts "3. salir"
    case option_layout()  
    when "1"
      sub_menu_cargar_tablero()    
    when "2"
      sub_menu_view_play()
    when "3"
      close()
    end
  end
  
  def sub_menu_cargar_tablero()
    layout()
    puts "Menu/cargar tablero"
    puts "1. cargar csv"
    puts "2. cargar random, ¡proximamente!"
    puts "3. Atras"
    case option_layout() 
    when "1"
      sub_load_csv()
      paint_board(@game.board)
      puts "Ya puedes empezar a Jugar, opcion 2 en el siguiente menu"
      puts "Presiona cualquier tecla para continuar"
      ConsoleViewHelper.input(label = '>>', indent = 0)
      menu()
    when "2"
      puts "proximamente!!!"
    when "3"
      menu()
    end  
  end

  def sub_load_csv
    puts "Cual es el nombre de tu archivo, el formato de entrada es: filename.csv"
    puts ColorizedString["Advertencia: El archivo debe de estar almacenado en la carpeta donde se esta ejecutando el programa"].colorize(:yellow)
    while(true) 
      print "Nombre del archivo "
      filename = ConsoleViewHelper.input(label = '>> ', indent = 0)
      begin
        @game.load_board({config: :file, filename: filename})
        puts " carga de tablero exitosa"
        break  
      rescue Exception => e
        puts ColorizedString["Error"].colorize(:red)
        puts "Ha ocurrido un problema cargando el archivo >> #{e} \n Por favor verifique que el archivo exista \n Ingrese el nombre del archivo" 
      end
    end
  end
    
  def validate_precense(cad, att)
    exp = /\A[a-zA-Z\s]+\z/
    until cad.match?(exp)
      puts "Lo sentimos pero el #{att} no puede contener numeros"
      puts "vuelve a digitar tu #{att}"
      cad = ConsoleViewHelper.input(label = '>>', indent = 0)
    end
    cad
  end

  def validate_precense_digit(cad, att)
    exp = /\A\d+\z/
    until cad.match?(exp)
      puts "Lo sentimos pero #{att} no puede contener numeros"
      puts "vuelve a digitar la #{att}"
      cad = gets.chomp
    end
    cad
  end
  
  def paint_board(board, config={water: {color: :cyan, symbol: 0}}, ship: {color: :light_black, symbol:'x'})
    print ColorizedString["|"].colorize(color: :black, background: :cyan) << ColorizedString["x".center(5)].colorize(color: :light_white, background: :black)
    (0...board.col).each{|i| print ColorizedString["|"].colorize(color: :black, background: :cyan) << ColorizedString["#{i}".center(5)].colorize(color: :light_white, background: :black)}
    print ColorizedString["|"].colorize(color: :black, background: :cyan)
    puts ""
    (0...board.row).each do |i|
      print ColorizedString["|"].colorize(color: :black, background: :cyan) << ColorizedString["#{i}".center(5)].colorize(color: :light_white, background: :black)
      (0...board.col).each do |j|
        case board.blocks[i][j].to_s
        when '0'
          print ColorizedString["|"].colorize(color: :black, background: :cyan) << ColorizedString[board.blocks[i][j].to_s.center(5)].colorize(color: config[:water][:color], background: config[:water][:color])
        when 'x'
          print ColorizedString["|"].colorize(color: :black, background: :cyan) << ColorizedString[board.blocks[i][j].to_s.center(5)].colorize(color: :light_black, background: :light_black)
        when '1'
          print ColorizedString["|"].colorize(color: :black, background: :cyan) << ColorizedString[board.blocks[i][j].to_s.center(5)].colorize(color: :red, background: :red)
        end
      end
      print ColorizedString["|"].colorize(color: :black, background: :cyan) 
      puts ""
    end
  end

  def paint_statistics
    puts "Estadisticas"
    puts ConsoleViewHelper.table([["Total de disparos", "Disparos fallidos"],[@game.total_shots.to_s, @game.failed_shots.to_s]], cell_width: 22)
  end
  
  def body_sub_play
    layout()
    puts "\n" * 2
    puts "#{@game.username.capitalize}, es hora de jugar!!!"
    puts "\n" * 2
    paint_board(@game.board)
    puts "\n" * 2
    puts "Asi va tu juego :)"
    paint_board(@game.board_result)
  end
  
  def sub_menu_view_play()
    body_sub_play()
    puts "Menu/Jugar"
    puts "1. Disparar"
    puts "2. Atras"
    puts "3. Salir"
    case option_layout()
    when "1"
      while(true)
        body_sub_play()
        paint_statistics()
        puts "Formato de disparo, ejemplo 1,2"
        pos = ConsoleViewHelper.input(label = 'Posicion en la que deseas jugar: >>', indent = 0)
        pos = pos.split(",").map(&:to_i)
        pp pos
        result = @game.shoot(pos)
        if result
          body_sub_play()
          paint_statistics() 
          puts "Barcos undido coordenadas"
          pp result
        end
        puts "Deseas continuar s/n"
        option = ConsoleViewHelper.input(label = '>>', indent = 0)
        if option == 'n'
          menu()
        end
      end
    when "2"
      menu()
    when "3"
      close()
    end
  end

  def close
    puts "Adios!!! #{","<< @game.username if @game.username}"
    sleep(5)
    exit
  end
end

# ----------main--------------

view = Gui.new
view.initial_presentation
view.menu